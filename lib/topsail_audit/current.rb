class Current < ActiveSupport::CurrentAttributes
  attribute :user
  attribute :request_uuid
end