class AssociationAuditLog < AuditLog
  
  belongs_to :associated, :polymorphic => true
  
  # strips_and_converts_blank_to_nil :associated_class_name, :association_name
  
  validates :association_name, :presence => true, :length => { :maximum => 200 }
  validates :associated_type,  :presence => true, :length => { :maximum => 100 }
  validates :associated_id,    :presence => true, :numericality => { :only_integer => true }
  
end