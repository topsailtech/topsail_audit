module TopsailAudit
  module MigrationExtensions
    module TableDefinitions
      def audit_cols(null: false,
                     index: false,
                     timestamps: true,
                     updated_by_column: true,
                     user_table: 'user_profile')
        # add created_by_id
        references :created_by,
                   null: null,
                   foreign_key: { to_table: user_table, name: "#{name}_cr_by_fk" },
                   index: index

        # add updated_by_id
        if updated_by_column
          references :updated_by,
                     null: null,
                     foreign_key: { to_table: user_table, name: "#{name}_up_by_fk" },
                     index: index
        end

        self.timestamps if timestamps
      end
    end
  end
end


if defined?(ActiveRecord::ConnectionAdapters::TableDefinition)
   ActiveRecord::ConnectionAdapters::TableDefinition.send :include, TopsailAudit::MigrationExtensions::TableDefinitions
end
