class AttributesAuditLog < AuditLog

  belongs_to :associated, :polymorphic => true

  def record_changes
    if changes_json.is_a?(String)
      JSON.parse(changes_json)
    else
      changes_json
    end
  end

end