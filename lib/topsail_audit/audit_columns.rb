# This file adds audit log column support to ActiveRecords

class ActiveRecord::Base
  # Add a "before_create" callback to automatically set 'created_by_id' to the ID of the
  # current user if the ActiveRecord has an "created_by_id" attribute.
  before_create :set_created_by_id

  # Add a "before_save" callback to automatically set 'updated_by_id' to the ID of the
  # current user if the ActiveRecord has an "updated_by_id" attribute.
  before_save :set_updated_by_id

  private

  # Sometimes the "changes" Hash with have fields that didn't really change, because
  # it gets confused by empty strings being nilified.  So as a workaround we filter
  # out changes that aren't really changes
  def real_changes
    real = changes
    real = {} if real.nil?
    real.delete_if { |key, value| value[0].nil? and value[1].blank? }
  end

  def set_created_by_id
    if self.respond_to?('created_by_id') and Current.user.present?
      self.created_by_id = Current.user.id
    end
  end

  def set_updated_by_id
    if self.respond_to?('updated_by_id') and Current.user.present? and (self.new_record? || !real_changes.empty?)
      self.updated_by_id = Current.user.id
    end
  end
end
