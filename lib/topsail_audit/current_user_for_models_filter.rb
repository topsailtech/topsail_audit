module Audit
  class CurrentUserForModelsFilter
    def around(controller)
      Current.user = controller.current_user_profile
      Current.request_uuid = controller.request.uuid
      yield
      # Current should handle this reset, but we do it here to be safe
      Current.user = nil
      Current.request_uuid = nil
    end
  end
end
