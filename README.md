# Topsail Audit

This plugin provides support for audit columns and basic audit log support using model and association callback.

* [Migration Helper for audit columns (_audit_cols_)](#Migration-Helper)
* ActsAsAudited concern

Changes to a record's attributes and associations are recorded in a audit_log table.
Model tables can also have :created_by_id and :updated_by_id columns that store who created/updated the row last

NOTE:  This plugin depends on *Current.user*
       When using Devise, this can be achieved through a around filter that is provided in this plugin as well:
       (example assumes that model UserProfile is configured for Devise)
       In the controller that calls "before_filter :authenticate_user_profile!", define 
              alias_method :current_user, :current_user_profile
       and right after "before_filter :authenticate_user_profile", add
              around_filter ::Audit::CurrentUserForModelsFilter.new

## Migration Helper
Syntax:
```ruby
t.audit_cols(null: false, 
             index: false, 
             timestamps: true, 
             updated_column: true,       # create column updated_by_id ?
             user_table: 'user_profile') # table to reference in foreign key
```
will add the following columns 

* ```created_by_id``` (automatically gets the *Current.user.id*)
* ```updated_by_id``` (automatically gets the *Current.user.id*)
* ```created_at```
* ```updated_at``` 
