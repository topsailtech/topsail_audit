class AuditLogMigration < ActiveRecord::Migration

  def self.up

    create_table :audit_log do |t|
      t.string     :type,         limit: 150, null: false
      t.string     :request_uuid
      t.references :audited,      polymorphic: true, null: false, index: true
      t.string     :action,       null: false, limit: 10
      t.jsonb      :all_atts
      t.jsonb      :changed_atts
      t.string     :association_name, limit: 200
      t.references :associated,       polymorphic: true

      t.references :created_by, class_name: 'UserProfile', foreign_key: { to_table: 'user_profile' }
      t.timestamp  :created_at, null: false, index: { order: { created_at: :desc } }

      t.index [ :created_by_id, :created_at ], order: {created_by_id: :asc, created_at: :desc}, name: 'audit_log_who_when_idx'
      t.index [:request_uuid, :created_at], order: { created_at: :desc }
    end

    execute "alter table audit_log add constraint audit_log_ck1 check (action in ('create', 'update', 'delete'))"
    execute "alter table audit_log add constraint audit_log_ck2 check (type in ('AttributesAuditLog', 'AssociationAuditLog'))"

  end

  def self.down
    drop_table :audit_log
  end

end